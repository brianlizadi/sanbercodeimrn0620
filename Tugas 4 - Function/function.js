// Jawaban Soal nomer 1

function teriak(){
    var teks = "Halo Sanbers!"
    return teks
}

var hasil = teriak()
console.log ("JAWABAN FUNCTION NO 1")
console.log (hasil +'\n')

// Jawaban Soal nomer 2

function kalikan(num1, num2){
    var kali = num1*num2
    return kali
}

var num1 = 12
var num2 = 4
var hasilkali = kalikan(num1, num2)
console.log ("JAWABAN FUNCTION NO 2")
console.log(hasilkali +'\n')

// Jawaban Soal nomer 3

function introduce(name, age, address, hobby){
    var sum = "Nama saya " + name + " ,Umur Saya " + age + " ,alamat saya di " + address + " ,dan saya punya hobby " + hobby
    return sum
}

var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"
var perkenalan = introduce(name, age, address, hobby)
console.log ("JAWABAN FUNCTION NO 3")
console.log(perkenalan)