//Jawaban Soal nomer 1

const golden = () => {
    console.log("this is golden!!")
  }
   
  golden()

//Jawaban Soal nomer 2

const newFunction =(firstName, lastName) => {
    return {
    firstName,
    lastName,
    fullName: () =>{
        console.log(firstName + " " + lastName)
        }
    }
}
 
//Driver Code 
newFunction("William", "Imoh").fullName() 

//Jawaban Soal nomer 3
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }


const{firstName, lastName, destination, occupation, spell} = newObject

const firstName1 = newObject.firstName;
const lastName1 = newObject.lastName;
const destination1 = newObject.destination;
const occupation1 = newObject.occupation;

console.log(firstName1);
console.log(lastName1)
console.log(destination1)
console.log(occupation1)

//Jawaban Soal nomer 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

//Jawaban Soal nomer 5
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, ` +  
    `consectetur adipiscing elit, ${planet} do eiusmod tempor ` +
    `incididunt ut labore et dolore magna aliqua. Ut enim` +
    ' ad minim veniam'
// Driver Code
console.log(before) 