//Jawaban Tugas Nomer 1

function arrayToObject(arr) {
    var now = new Date()
    var tahun = now.getFullYear()
    var data1 
    var sum1


    for (var i = 0; i <= arr.length; i++){
        var ttl = arr[i][3]        
        
        if(tahun > ttl){
            var umur = tahun - ttl
            data1 = '{\n'+'    firstName: ' + arr[i][0] + '\n    lastname: ' + arr[i][1] + '\n    gender: ' + arr[i][2] + '\n    age: ' + umur + '\n}'
            sum1 = i+1  + '. ' + arr[i][0] + ' ' + arr[i][1] + ': '
            var hasil = sum1 + data1
            return hasil
        }else{
            data2 = '{\n'+'    firstName: ' + arr[i][0] + '\n    lastname: ' + arr[i][1] + '\n    gender: ' + arr[i][2] + '\n    age: Invalid Birth Year' + '\n}'
            sum2 = i+1  + '. ' + arr[i][0] + ' ' + arr[i][1] + ': '
            var hasil2 = sum2 + data2
            return hasil2
        }
    }
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] , ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
var people1 =  [["Natasha", "Romanoff", "female"], ["Bruce", "Banner", "male", 1975], ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
var people2 =  [["Tony", "Stark", "male", 1980], ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"], ["Pepper", "Pots", "female", 2023] ]
var people3 =  [["Pepper", "Pots", "female", 2023], ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"], ["Tony", "Stark", "male", 1980], ]

console.log(arrayToObject(people))
console.log(arrayToObject(people1))
console.log(arrayToObject(people2))
console.log(arrayToObject(people3))

// Jawaban Soal nomer 2

function shoppingTime(memberId, money) {
    var nabar = ['Sepatu Stacattu', 'Baju Zoro', 'Baju H&N', 'Sweater Uniklooh', 'Casing Handphone']
    var harga = ['1500000', '500000', '250000', '175000', '50000']
    
    var prwk = []
    var Stacattu = 1500000
    var Zoro = 500000
    var HNN = 250000
    var Uniklooh =175000
    var Handphon = 50000

    var tot = 1500000 + 500000 + 250000 + 175000 + 50000

    if(memberId == '' || memberId == 0 || memberId == null){
        var cot = 'Mohon maaf, toko X hanya berlaku untuk member saja'
        return cot
    }else if(money < 50000){
        var cot1 = 'Mohon maaf, uang tidak cukup'
        return cot1
    }else if(money >= tot ){
        var sisa1 = money - tot
        var ob1 =  { 'memberId': memberId,
        'money': money,
        'listPurchased': nabar.toString(),
         'changeMoney': sisa1 }
        
         return ob1
    }else if (money < Stacattu && money > Zoro){
        var sisa2 = money - Zoro 
        var ob2 =  { 'memberId': memberId,
        'money': money,
        'listPurchased': 0,
         'changeMoney': sisa2 }

         return ob2
    }else if (money < Zoro && money > HNN){
        var sisa3 = money - HNN 
        var ob3 =  { 'memberId': memberId,
        'money': money,
        'listPurchased': 0,
         'changeMoney': sisa3 }

         return ob3
    }else if (money > Handphon && money< Uniklooh){
        var sisa4 = money - Handphon
        var ob4 =  { 'memberId': memberId,
        'money': money,
        'listPurchased': nabar[4],
         'changeMoney': sisa4 }

         return ob4
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); 
console.log(shoppingTime('234JdhweRxa53', 15000)); 
console.log(shoppingTime());

//Jawaban Soal nomer 3
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    

    for(var i = 0; i < arrPenumpang.length; i++){
        
        if( arrPenumpang[i][1] + arrPenumpang[i][2] == rute[0] + rute[4]){
            var uang = 8000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng

        }else if ( arrPenumpang[i][1] + arrPenumpang[i][2] == rute[1] + rute[5]){
            var uang = 8000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng
        }else if ( arrPenumpang[i][1] + arrPenumpang[i][2] == rute[0] + rute[3]){
            var uang = 6000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng
        }else if ( arrPenumpang[i][1] + arrPenumpang[i][2] == rute[1] + rute[4]){
            var uang = 6000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng
        }else if ( arrPenumpang[i][1] + arrPenumpang[i][2] == rute[2] + rute[5]){
            var uang = 6000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng
        }else if ( arrPenumpang[i][1] + arrPenumpang[i][2] == rute[0] + rute[2]){
            var uang = 4000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng
        }else if ( arrPenumpang[i][1] + arrPenumpang[i][2] == rute[3] + rute[5]){
            var uang = 4000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng
        }else if ( arrPenumpang[i][1] + arrPenumpang[i][2] == rute[2] + rute[4]){
            var uang = 8000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng
        }else if ( arrPenumpang[i][1] + arrPenumpang[i][2] == rute[1] + rute[5]){
            var uang = 8000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng
        }else if ( arrPenumpang[i][1] + arrPenumpang[i][2] === rute[0], rute[1] && rute[1], rute[2] && rute[2], rute[3] && rute[3], rute[4] && rute[4], rute[5]){
            var uang = 2000
            var pnmpng = [{'penumpang': arrPenumpang[i][0], 'naikDari': arrPenumpang[i][1], 'tujuan': arrPenumpang[i][2], 'bayar': uang}]
            return pnmpng

        }

    }
    
  }

  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  console.log(naikAngkot([['Icha', 'A', 'B'], ['Dimitri', 'B', 'F']]));