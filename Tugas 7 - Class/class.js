//Jawaban Soal nomer 1

class Animal{
    constructor(name){
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false;
    }

}

var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal{
    constructor(name){
        super(name);
        this.suara = "Auooo";
    }
    yell(){
        return this.suara
    }
}

class Frog extends Animal{
    constructor(name){
        super(name);
        this.jumppp = "hop hop";
    }
    jump(){
        return this.jumppp
    }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.yell()) // "Auooo"
 
var kodok = new Frog("buduk")
console.log(kodok.jump()) // "hop hop" 

//Jawaban soal nomer 2

console.log('\nMENUNJUKAN PUKUL')
class Clock{
    constructor({template}){
        this.mulai = {template}
    }
    render(){
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
   
        return console.log(hours +':'+ mins +':'+ secs)
    }
    stop(){
        this.berhenti = clearInterval(timer);
    }
    start(){
        this.render()
        timer = setInterval(this.render, 1000)
        
        return timer
    }
}
var timer;
var clock = new Clock({template: 'h:m:s'});
clock.start()











