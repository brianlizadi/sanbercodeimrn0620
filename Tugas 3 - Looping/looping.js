// Jawaban Soal nomer 1
var loop_1 = 2;
var loop_2 = 20;
console.log("\nLOOPING PERTAMA");

while (loop_1 <= 20){
    console.log(loop_1 + ' - I love coding');
    loop_1 +=2;
}

console.log("\nLOOPING KEDUA");

while (loop_2 > 0){
    console.log(loop_2 + ' - I will become a mobile developer');
    loop_2 -=2;
}

// Jawaban Soal nomer 2

console.log('\n' + "LOOPING MENGGUNAKAN FOR")
for(var i = 1; i <= 20; i++ ) {
    if (i%3 && i%2){
        console.log(i + " - Santai");
    }else if (i%2){
        console.log(i + " - I Love Coding");
    }else{
        console.log(i + " - Berkualitas");
    }
}


// Jawaban Soal nomer 3

console.log('\n' + "MEMBUAT KOTAK")
var block='#';

for(var i = 0; i < 4; i++){
    for(var a = 0; a <= 8; a++){
        if (a == 8 ) {
            console.log(Array(a+1).join(block))
        }else{
            null
        }
        
    }
}

// Jawaban Soal nomer 4

console.log('\n' + "MEMBUAT SEGITIGA")
var block='';

for(var i = 0; i < 7; i++){
    console.log(block+="#");
}


// Jawaban Soal nomer 5

console.log('\n' + "MEMBUAT PAPAN CATUR")
var block1=' #';
var block2='# '

for(var i = 0; i < 8; i++){
    for(var a = 0; a <= 4; a++){
        if (a == 4 && i%2 == 0 ) {
            console.log(Array(a+1).join(block1))
        }else if(a == 4 && i%2 == 1  ){
            console.log(Array(a+1).join(block2))
        }else{
            null;
        }
        
    }
}